## 资料链接
开发板[Sipeed Tang nano 20k Wiki](https://wiki.sipeed.com/hardware/zh/tang/tang-nano-20k/nano-20k.html)  

## 引脚定义
所有IO均为LVCMOS33
### 主要功能
|引脚号|功能|
|-|-|
|4|27MHz单端时钟|
|69|BL616 UART Rx|
|70|BL616 UART Tx|
|87|S2按键，默认下拉|
|88|S1按键，默认下拉|


### 其他
